﻿using Xunit;
using CoreHWApp;

namespace Tests
{
    public class ArrayTests
    {

        [Fact]
        public void sumAllPosotiveElemets_WhenLengtArrayZero()
        {
            Arrays arr = new Arrays();
            int sum2 = arr.sumPositiveElementsArray(new int[] { });
            Assert.Equal(0, sum2);
            
        }

        [Fact]
        public void SumAllPosotiveElemets_WhenValuesArrayNegative()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumPositiveElementsArray(new int[] { -1, -2, -3 });
            Assert.Equal(0, sum);
        }

        [Fact]
        public void SumAllPosotiveElemets_WhenValuesArrayPositive()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumPositiveElementsArray(new int[] { 1, -2, 4, -8, -9, 0, 2 });
            Assert.Equal(7, sum);
        }

        [Fact]
        public void sumBetweenFirstAndLastElemet_WhenLengtArrayZero()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumBetweenFirstAndLastElemet(new int[] { });
            Assert.Equal(0, sum);
        }

        [Fact]
        public void sumBetweenFirstAndLastElemet_WhenLengtArrayOnePositive()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumBetweenFirstAndLastElemet(new int[] { 1 });
            Assert.Equal(1, sum);
        }

        [Fact]
        public void sumBetweenFirstAndLastElemet_WhenLengtArrayOneNegative()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumBetweenFirstAndLastElemet(new int[] { -1 });
            Assert.Equal(0, sum);
        }

        [Fact]
        public void sumBetweenFirstAndLastElemet__WhenValuesArrayNegative()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumBetweenFirstAndLastElemet(new int[] { -1, -2 });
            Assert.Equal(0, sum);
        }

        [Fact]
        public void sumBetweenFirstAndLastElemet_WhenValuesArrayPositive()
        {
            Arrays arr = new Arrays();
            int sum = arr.sumBetweenFirstAndLastElemet(new int[] { -1, -1, 2, 3, 2, -1 });
            Assert.Equal(7, sum);
        }

        [Fact]
        public void changeArrayZeroElLeftOf_WhenArrayLengthZero()
        {
            Arrays arr = new Arrays();
            int[] resultArr = arr.changeArrayZeroElLeftOf(new int[] { });
            Assert.Equal(new int[]{ },resultArr);
        }

        [Fact]
        public void changeArrayZeroElLeftOf_WhenInArrayNotFoundZero()
        {
            Arrays arr = new Arrays();
            int[] resultArr = arr.changeArrayZeroElLeftOf(new int[] { 1, 3, 4 });
            Assert.Equal(new int[] { 1, 3, 4 }, resultArr);
        }

        [Fact]
        public void changeArrayZeroElLeftOf_WhenInArrayFoundZero()
        {
            Arrays arr = new Arrays();
            int[] resultArr = arr.changeArrayZeroElLeftOf(new int[] { 1, 0, 3, 4, 0 });
            Assert.Equal(new int[] {0, 0, 3, 4, 1 }, resultArr);
        }

    }
}