﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreHWApp
{
    public class Arrays
    {
        public int sumPositiveElementsArray(int[] array)
        {
            if (array.Length == 0) return 0;
            int sum = 0;
            
            foreach(int element in array)
            {
                if(element > 0)
                {
                    sum += element;
                }
            }

            if (sum == 0) return 0;

            return sum;
        }

        public int sumBetweenFirstAndLastElemet(int[] array)
        {
            if (array.Length == 0) return 0;
            int firstIndex = 0, lastIndex = 0, sum = 0;
            bool foundFirstElement = false;

            for(int i = 0; i< array.Length; i++)
            {
                if (!foundFirstElement)
                {
                    if (array[i] > 0)
                    {
                        firstIndex = i;
                        foundFirstElement = true;
                    }
                    else
                    {
                        firstIndex = lastIndex = i;
                    }
                }
                else
                {
                    if (array[i] > 0)
                    {
                        lastIndex = i; ;
                    }
                }

            }

            for(int i = firstIndex; i < lastIndex + 1; i++)
            {
                sum += array[i];
            }

            if (sum <= 0) return 0;

            return sum;
        }

        public int[] changeArrayZeroElLeftOf(int[] array)
        {
            int leftOffsetIndex = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 0)
                {
                    array[i] = array[leftOffsetIndex];
                    array[leftOffsetIndex] = 0;
                    leftOffsetIndex++;
                }
            }

            return array;
        }

    }
}
